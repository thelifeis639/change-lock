# frozen_string_literal: true

module ChangeLock
  class CheckFailed < StandardError
  end

  class Check
    include ::SemanticLogger::Loggable

    def initialize(limit_tags: [], slack_channels: [])
      @limit_tags = limit_tags.map(&:downcase)
      @slack_channels = slack_channels
      @cfg = ChangeLock::Config.configuration
    end

    def valid?
      check_periods do |start, finish, tags|
        next unless check_tags?(tags)

        logger.debug('Checking changelock period',
                     project_name: project_name,
                     start: start,
                     finish: finish,
                     tags: tags,
                     channels: @slack_channels,
                     limit_tags: @limit_tags)

        next unless ChangeLock::Period.new(
          start, finish,
          slack_channels: @slack_channels
        ).changelock?

        logger.info('ChangeLock check failed',
                    start: start, finish: finish)

        if change_lock_override?
          logger.warn('CHANGE_LOCK_OVERRIDE is set, not failing')
          next
        end

        return false
      end
      true
    end

    private

    def check_periods
      @cfg['changelock_periods'].each do |period|
        start = period['start']
        finish = period['finish']
        tags = period.fetch('tags', []).map(&:downcase)

        yield(start, finish, tags)
      end
    end

    def check_tags?(tags)
      # If no limit tags are specified as arguments
      # or if there are no tags
      # explicitly set on the period, evaluate the window
      return true if @limit_tags.empty? || tags.empty?

      # If the limit tags specified match at least one tag
      # in the config, evaluate the window
      (@limit_tags & tags).any?
    end

    def project_name
      ENV.fetch('CI_PROJECT_NAME', 'unknown project')
    end

    def slack_cfg
      @cfg.fetch('slack', [])
    end

    def change_lock_override?
      ENV.key?('CHANGE_LOCK_OVERRIDE')
    end
  end
end
