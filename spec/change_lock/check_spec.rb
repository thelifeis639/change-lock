# frozen_string_literal: true

require 'spec_helper'

shared_context 'with fake slack' do
  let(:fake_client) { instance_spy(Slack::Web::Client) }

  around do |ex|
    ClimateControl.modify(SLACK_TOKEN: 'fake-token') do
      ex.run
    end
  end

  before do
    allow(Slack::Web::Client).to receive(:new).and_return(fake_client)
  end
end

describe ChangeLock::Check do
  context 'when no tag arguments are specified with the default configuration' do
    subject(:check) { described_class.new(slack_channels: ['#some-channel1', '#some-channel2']) }

    describe '#valid?' do
      include_context 'with fake slack'

      it 'passes the check for a date outside of all windows' do
        Timecop.freeze(Time.utc(2020, 12, 1)) do
          expect(check.valid?).to be(true)
        end
        Timecop.freeze(Time.utc(2020, 11, 19)) do
          expect(check.valid?).to be(true)
        end
      end

      it 'fails the check early in the weekend' do
        # 2020-12-5 is a Saturday
        Timecop.freeze(Time.utc(2020, 12, 5)) do
          expect(check.valid?).to be(false)
          expect(fake_client).to have_received(:chat_postMessage).twice
        end
      end

      it 'fails the check on a thanksgiving and blackfriday 2020' do
        Timecop.freeze(Time.utc(2020, 11, 26)) do
          expect(check.valid?).to be(false)
        end
        Timecop.freeze(Time.utc(2020, 11, 27)) do
          expect(check.valid?).to be(false)
        end
        expect(fake_client).to have_received(:chat_postMessage).exactly(4).times
      end

      it 'passes the check on thanksgiving-eve at 23:59:59' do
        Timecop.freeze(Time.utc(2020, 11, 25, 23, 59, 59)) do
          expect(check.valid?).to be(true)
        end
      end

      it 'fails the check on Christmas 00:00:00 and day after' do
        Timecop.freeze(Time.utc(2020, 12, 25)) do
          expect(check.valid?).to be(false)
        end
        Timecop.freeze(Time.utc(2020, 12, 26)) do
          expect(check.valid?).to be(false)
        end
        expect(fake_client).to have_received(:chat_postMessage).exactly(4).times
      end

      it 'passes the check on Christmas Eve 23:59:59' do
        Timecop.freeze(Time.utc(2020, 12, 24, 23, 59, 59)) do
          expect(check.valid?).to be(true)
        end
      end

      it 'fails the changelock check on New Years' do
        Timecop.freeze(Time.utc(2020, 1, 1)) do
          expect(check.valid?).to be(false)
          expect(fake_client).to have_received(:chat_postMessage).twice
        end
      end
    end
  end

  context 'when tag is delivery with the default configuration' do
    subject(:check_delivery) { described_class.new(limit_tags: ['delivery'], slack_channels: ['#some-channel1', '#some-channel2']) }

    describe '#valid?' do
      include_context 'with fake slack'

      it 'fails the check very late in the weekend window for delivery' do
        # 2020-12-7 is a Monday; 5:55am should be within the window
        Timecop.freeze(Time.utc(2020, 12, 7, 5, 55, 0)) do
          expect(check_delivery.valid?).to be(false)
          expect(fake_client).to have_received(:chat_postMessage).twice
        end
      end

      it 'fails the check towards the end of the weekend window for delivery' do
        # 2020-12-7 is a Monday; 1am should be within the delivery window but
        # after when infra starts being allowed.
        Timecop.freeze(Time.utc(2020, 12, 7, 1, 0, 0)) do
          expect(check_delivery.valid?).to be(false)
          expect(fake_client).to have_received(:chat_postMessage).twice
        end
      end
    end
  end

  context 'when tag is infra with the default configuration' do
    subject(:check_infra) { described_class.new(limit_tags: ['infra'], slack_channels: ['#some-channel1', '#some-channel2']) }

    describe '#valid?' do
      include_context 'with fake slack'

      it 'passes the check early in the week for APAC infra' do
        # 2020-12-7 is a Monday, 1am should be outside the window for infra
        # but still within the window for delivery
        Timecop.freeze(Time.utc(2020, 12, 7, 1, 0, 0)) do
          expect(check_infra.valid?).to be(true)
          expect(fake_client).to have_received(:chat_postMessage).exactly(0).times
        end
      end
    end
  end
end
