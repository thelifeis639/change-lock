# frozen_string_literal: true

require 'spec_helper'

describe ChangeLock::Period do
  let(:fake_client) { instance_spy(Slack::Web::Client) }

  def ci_vars
    {
      CI_JOB_ID: '50',
      CI_JOB_NAME: 'gitlab-qa',
      CI_JOB_URL: 'https://example.com/gitlab-org/gitlab-foss/-/jobs/50',
      CI_PIPELINE_URL: 'https://example.com/gitlab-org/gitlab-foss/pipelines/10',
      CI_PROJECT_DIR: '/builds/gitlab-org/gitlab-foss',
      CI_PROJECT_NAME: 'gitlab-foss',
      CI_PROJECT_PATH: 'gitlab-org/gitlab-foss',
      CI_PROJECT_URL: 'https://example.com/gitlab-org/gitlab-foss',
      SLACK_TOKEN: 'fake-token'
    }
  end

  describe '#changelock?' do
    around do |ex|
      ClimateControl.modify(ci_vars) do
        Timecop.freeze(Time.utc(2020, 12, 15, 12, 0)) do
          ex.run
        end
      end
    end

    before do
      allow(Slack::Web::Client).to receive(:new).and_return(fake_client)
    end

    it 'reports inside the changelock window for a date' do
      expect(described_class.new('2020-12-15 00:00:00 UTC', '2020-12-16 23:59:59 UTC').changelock?).to be(true)
    end

    it 'reports inside the changelock window for a date and sends a slack notification' do
      expect(described_class.new('2020-12-15 00:00:00 UTC', '2020-12-16 23:59:59 UTC',
                                 slack_channels: ['#test-channel1']).changelock?).to be(true)
      expect(fake_client).to have_received(:chat_postMessage).with(
        channel: '#test-channel1',
        text: "⚠️ *gitlab-foss* ChangeLock check failed\n\n2020-12-15 12:00:00 UTC is between:\n*2020-12-15 00:00:00 UTC* and *2020-12-16 23:59:59 UTC*\n\nRestart the <https://example.com/gitlab-org/gitlab-foss/-/jobs/50|job> or view the <https://example.com/gitlab-org/gitlab-foss/pipelines/10|pipeline>. Set CHANGE_LOCK_OVERRIDE=true as a CI variable to override.\n_<https://gitlab.com/gitlab-com/gl-infra/change-lock/blob/master/config/changelock.yml|edit the changelock periods>_\n\n"
      )
    end

    it 'reports outside the changelock window for a date' do
      expect(described_class.new('2020-12-10 00:00:00 UTC', '2020-12-11 23:59:59 UTC').changelock?).to be(false)
    end

    it 'reports outside the changelock window for a date and does not send a slack notification' do
      expect(described_class.new('2020-12-10 00:00:00 UTC', '2020-12-11 23:59:59 UTC',
                                 slack_channels: ['#test-channel1', '#test-channel2']).changelock?).to be(false)
      expect(fake_client).not_to have_received(:chat_postMessage)
    end

    it 'raises an exception for an invalid period specifier' do
      expect { described_class.new('9000', '2020-12-11 23:59:59 UTC').changelock? }.to raise_error(StandardError)
    end

    it 'raises an exception if start > end' do
      expect { described_class.new('2020-12-16 23:59:59 UTC', '2020-12-15 00:00:00 UTC').changelock? }.to raise_error(StandardError)
    end

    it 'raises an exception if start == end' do
      expect { described_class.new('2020-12-16 23:59:59 UTC', '2020-12-16 23:59:59 UTC').changelock? }.to raise_error(StandardError)
    end
  end
end
