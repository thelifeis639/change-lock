# frozen_string_literal: true

# SimpleCov needs to be loaded before everything else
require_relative 'support/simplecov'
require_relative 'support/stub_env'
require_relative 'support/webmock'
require_relative '../lib/changelock'
require 'timecop'

RSpec.configure do |config|
  config.run_all_when_everything_filtered = true
  config.filter_run :focus
  config.order = 'random'
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
end

RSpec::Support::ObjectFormatter.default_instance.max_formatted_output_length = 1000
